<?php

namespace Drupal\krumong;

use Drupal\krumong\ServiceCache\ServiceCacheInterface;

abstract class MainAbstract implements MainInterface {

  /**
   * @var ServiceCacheInterface
   */
  private $serviceCache;

  /**
   * @param ServiceCacheInterface $serviceCache
   */
  function __construct(ServiceCacheInterface $serviceCache) {
    $this->serviceCache = $serviceCache;
  }

  /**
   * Determine where dpm() and friends are called from.
   *
   * @return array|null
   *   Associative array with 'file' and 'line' information.
   */
  function calledFrom() {
    $call = $this->calledFromCall(0);
    if (!isset($call)) {
      return NULL;
    }
    return array(
      'file' => $call['file'],
      'line' => $call['line'],
    );
  }

  /**
   * @param int $k
   *
   * @return array|null
   *   A backtrace item or NULL.
   */
  function calledFromCall($k = 1) {
    $trace = debug_backtrace();
    // Find the first trace item that ...
    foreach ($trace as $i => $call) {
      if (!isset($call['file']) || !isset($call['line'])) {
        continue;
      }
      $filename = basename($call['file']);
      if ($filename === 'krumong.namespaced.inc' || $filename === 'devel.module') {
        continue;
      }
      if (isset($trace[$i + 1]['class']) && preg_match('#^Drupal\\\\krumong\\\\#', $trace[$i + 1]['class'])) {
        continue;
      }
      return isset($trace[$i + $k])
        ? $trace[$i + $k]
        : NULL;
    }

    return NULL;
  }

  /**
   * @param string $key
   *
   * @return mixed
   */
  function __get($key) {
    return $this->serviceCache->__get($key);
  }
}
