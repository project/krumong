<?php

namespace Drupal\krumong\Output;


/**
 * Ouch, that's a bit too much abstraction.
 * Let's see how the rest shapes up, then we change this.
 */
class StdOut {

  /**
   * @var CssJs
   */
  protected $cssJs;

  /**
   * @param CssJs $cssJs
   */
  function __construct(CssJs $cssJs) {
    $this->cssJs = $cssJs;
  }

  /**
   * @param string $html
   */
  function html($html) {
    $this->cssJs->printCssJsOnce();
    print $html;
  }
}
