<?php

namespace Drupal\krumong\ServiceCache;

use Drupal\krumong as m;
use Drupal\krumong\Devel;
use Drupal\krumong\MainNoAccess;
use Drupal\krumong\Main;
use Drupal\krumong\Output\AjaxMessageSystem;
use Drupal\krumong\Output\CssJs;
use Drupal\krumong\Output\SessionRequestCache;
use Drupal\krumong\Output\StdOut;


class ServiceFactory {

  /**
   * @param $key
   * @param ServiceCacheInterface $cache
   *
   * @return mixed
   */
  function createService($key, ServiceCacheInterface $cache) {
    $method = 'get_' . $key;
    return $this->$method($cache);
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return Devel
   */
  protected function get_devel(ServiceCacheInterface $cache) {
    return new Devel($cache->main, $cache->mainAccessChecked);
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return \Drupal\krumong\MainInterface
   */
  protected function get_mainAccessChecked(ServiceCacheInterface $cache) {
    if (user_access('access devel information')) {
      return $cache->main;
    }
    else {
      return new MainNoAccess($cache);
    }
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return Main
   */
  protected function get_public(ServiceCacheInterface $cache) {
    return $cache->main;
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return Main
   */
  protected function get_main(ServiceCacheInterface $cache) {
    return new Main($cache, $cache->requestCache, $cache->stdOut, $cache->cssJs);
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return CssJs
   */
  protected function get_cssJs(ServiceCacheInterface $cache) {
    return new CssJs;
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return SessionRequestCache
   */
  protected function get_requestCache(ServiceCacheInterface $cache) {
    return new SessionRequestCache('krumong_messages', $cache->cssJs);
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return AjaxMessageSystem
   */
  protected function get_ajaxMessageSystem(ServiceCacheInterface $cache) {
    $key = 'krumong_ajax_messages';
    if (!isset($_SESSION[$key])) {
      $_SESSION[$key] = array();
    }
    return new AjaxMessageSystem($_SESSION[$key]);
  }

  /**
   * @param ServiceCacheInterface $cache
   *
   * @return StdOut
   */
  protected function get_stdOut(ServiceCacheInterface $cache) {
    return new StdOut($cache->cssJs);
  }
}
