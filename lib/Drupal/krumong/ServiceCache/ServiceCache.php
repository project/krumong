<?php

namespace Drupal\krumong\ServiceCache;


/**
 * ServiceCache + ServiceFactory is the little brother of a DIC.
 * It is perfectly sufficient to manage the dependencies within a single module.
 */
class ServiceCache implements ServiceCacheInterface {

  /**
   * @var array
   */
  protected $cache = array();

  /**
   * @var ServiceFactory
   */
  protected $factory;

  /**
   * @param ServiceFactory $factory
   */
  function __construct(ServiceFactory $factory) {
    $this->factory = $factory;
  }

  /**
   * @param $key
   *
   * @return mixed
   */
  function __get($key) {
    if (!isset($this->cache[$key])) {
      $this->cache[$key] = $this->factory->createService($key, $this);
    }
    return $this->cache[$key];
  }
}
