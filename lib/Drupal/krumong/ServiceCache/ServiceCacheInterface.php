<?php


namespace Drupal\krumong\ServiceCache;

use Drupal\krumong\Devel;
use Drupal\krumong\MainInterface;
use Drupal\krumong\Output\AjaxMessageSystem;
use Drupal\krumong\Output\CssJs;
use Drupal\krumong\Output\SessionRequestCache;
use Drupal\krumong\Output\StdOut;

/**
 * @property Devel $devel
 * @property MainInterface $mainAccessChecked
 * @property MainInterface $public
 * @property MainInterface $main
 * @property CssJs $cssJs
 * @property SessionRequestCache $requestCache
 * @property AjaxMessageSystem $ajaxMessageSystem
 * @property StdOut $stdOut
 */
interface ServiceCacheInterface {

  /**
   * @param string $key
   *
   * @return mixed
   */
  public function __get($key);
} 
