<?php

namespace Drupal\krumong\Tests;
use Drupal\krumong\TreeRenderer_PrintR;
use Drupal\krumong\TreeTheme_PrintR;
use \stdClass;

class BasicUnitTest extends \DrupalUnitTestCase {

  static function getInfo() {
    return array(
      'name' => 'Krumo NG basic unit test',
      'description' => '...',
      'group' => 'Krumo NG',
    );
  }

  /**
   * Test that TreeRenderer_PrintR + TreeTheme_PrintR behave like print_r().
   */
  function testPrintR() {

    $resource = fopen(__FILE__, 'r');
    $data = $this->buildTestArray($resource);

    $theme = new TreeTheme_PrintR();
    $renderer = new TreeRenderer_PrintR($theme);
    $mimick = $renderer->render($data);

    $print_r = print_r($data, TRUE);
    $this->assertEqual($mimick, $print_r, "TreeRenderer_PrintR + TreeTheme_PrintR must mimick print_r().");
  }

  /**
   * Build an arbitrary-ish test array with some circular references.
   *
   * @param resource $resource
   *   The resource has to be passed in, because the caller is responsible of
   *   closing it after use.
   *
   * @return array
   *   An array for testing.
   */
  protected function buildTestArray($resource) {

    // Basic setup.
    $data = array(
      'testResource' => $resource,
      'testObject' => new stdClass,
      'testObject2' => new stdClass,
      'testObject3' => new stdClass,
      'testArray' => array(
        'testNestedArray' => array()
      ),
    );

    // Circular array reference.
    $data['testArray']['testNestedArray']['testCircularReference'] = &$data['x'];

    // Mirrored object pointer.
    $data['testObjectMirror'] = $data['testObject'];

    // Two-step circular object pointers.
    $data['testObject2']->testObject3 = $data['testObject3'];
    $data['testObject3']->testObject2 = $data['testObject2'];

    return $data;
  }

  /**
   * Test the reference side effect on objects.
   */
  function testObjectSideEffect() {
    $obj = new \stdClass;
    $obj->val = array(0);
    $this->doTestSideEffect($obj, TRUE);
  }

  /**
   * Test the reference side effect on arrays.
   */
  function testArraySideEffect() {
    $arr = array('val' => array(0));
    $this->doTestSideEffect($arr);
  }

  /**
   * Actually do the test with either an object or array.
   */
  protected function doTestSideEffect($obj, $inverse_on_devel = FALSE) {
    $this->assertChildIsNotReference($obj, 'val', 'creation');

    // Test krumong()->dpm() side effect.
    krumong()->jsPrefixed($obj);
    $this->assertChildIsNotReference($obj, 'val', 'krumong()->dpm()');

    // Test devel dpm() / krumo::dump() side effect.
    if (1
      && \module_exists('devel')
      && ($devel_path = drupal_get_path('module', 'devel'))
      && ($krumo_file = $devel_path . '/krumo/class.krumo.php')
      && file_exists($krumo_file)
    ) {
      require_once $krumo_file;
      if (class_exists('krumo', FALSE)) {
        ob_start();
        \krumo::dump($obj);
        ob_end_clean();
        $this->assertChildIsNotReference($obj, 'val', 'devel dpm()', $inverse_on_devel);
      }
    }
  }

  /**
   * Assert that $obj->$key / $obj[$key] is not a reference.
   *
   * @param object|array $obj
   *   The object or array.
   * @param string $key
   *   The key.
   * @param string $action
   *   String describing what happened before the assertion.
   * @param boolean $inverse
   *   If TRUE, we already assume that it *will* be a reference,
   *   and the test just confirms the sad fact.
   */
  protected function assertChildIsNotReference($obj, $key, $action, $inverse = FALSE) {
    if (is_object($obj)) {
      $type = 'An object variable';
      $is_ref = $this->objectVarIsReference($obj, $key);
    }
    else {
      $type = 'An array value';
      $is_ref = $this->arrayValueIsReference($obj, $key);
    }
    if (!$inverse) {
      $this->assertTrue(!$is_ref, "$type may not become a reference after $action.");
    }
    else {
      $this->assertTrue($is_ref, "$type sadly does become a reference after $action.");
    }
  }

  /**
   * Check if an object attribute ate the given key is a reference.
   *
   * @param object $obj
   *   The object to test.
   * @param int|string $key
   *   Attribute name to test.
   *
   * @return boolean
   *   TRUE, if the $obj->$key is a reference.
   *   FALSE, otherwise.
   */
  protected function objectVarIsReference($obj, $key) {
    $orig = $obj->$key;
    $cl = clone $obj;
    $cl->$key = is_string($orig) ? 99 : 'xx';
    $is_ref = ($obj->$key !== $orig);
    $cl->$key = $orig;
    return $is_ref;
  }

  /**
   * Check if an array value at a given key is a reference.
   *
   * @param array $arr
   *   The array to test.
   * @param int|string $key
   *   Array key to test.
   *
   * @return boolean
   *   TRUE, if the $arr[$key] is a reference.
   *   FALSE, otherwise.
   */
  protected function arrayValueIsReference($arr, $key) {
    $orig = $arr[$key];
    $cl = $arr;
    $cl[$key] = is_string($orig) ? 99 : 'xx';
    $is_ref = ($arr[$key] !== $orig);
    $cl[$key] = $orig;
    return $is_ref;
  }
}
