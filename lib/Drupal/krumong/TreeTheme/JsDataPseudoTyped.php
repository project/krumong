<?php

namespace Drupal\krumong;


/**
 * Render the tree as nested javascript value/object.
 * This is not a simple json_encode(), and it is not valid json, so only works
 * if run as full javascript.
 *
 * Will use "pseudo types" that show up nicely in console.log.
 * E.g. console.log will say "PHP.SomeNamespace\SubNamespace\ClassName" for the
 * type of the object.
 */
class TreeTheme_JsDataPseudoTyped implements TreeTheme_Interface {

  /**
   * @var array
   */
  protected $classes = array();

  /**
   * @var array
   */
  protected $recursions = array();

  /**
   * @param array $array
   * @param array $position
   * @param array $children
   *
   * @return string
   */
  function renderArray(array $array, array $position, array $children) {
    $rendered = $this->renderKeyValueData($children, '[', ']');
    return "a($rendered)";
  }

  /**
   * @param object $object
   * @param array $position
   * @param array $children
   *
   * @return string
   */
  function renderObject($object, array $position, array $children) {
    $class = json_encode(get_class($object));
    $this->classes[$class] = TRUE;
    $rendered = $this->renderKeyValueData($children, '->', '');
    return "o($class, $rendered)";
  }

  /**
   * @param $data
   * @param $prefix
   * @param $suffix
   *
   * @return string
   */
  protected function renderKeyValueData($data, $prefix, $suffix) {
    $parts = array();
    foreach ($data as $k => $v) {
      $parts[] = '  ' . json_encode($prefix . $k . $suffix) . ': ' . $v;
    }
    return "{\n" . implode(",\n", $parts) . "\n}";
  }

  /**
   * @param resource $resource
   * @param array $position
   *
   * @return string
   */
  function renderResource($resource, array $position) {
    $type = get_resource_type($resource);
    // Use the __toString() in the resource.
    return json_encode("resource ($type): $resource");
  }

  /**
   * @param mixed $value
   * @param array $position
   *
   * @return string
   */
  function renderPrimitive($value, array $position) {
    return json_encode($value);
  }

  /**
   * @param mixed $value
   * @param array $position
   * @param array $original_position
   *
   * @return string
   */
  function renderRecursion($value, array $position, array $original_position) {
    $json = json_encode($original_position);
    $json_json = json_encode($json);
    $this->recursions[$json_json] = TRUE;
    return "new Recursion[$json_json]";
  }

  /**
   * @param string $string
   * @param string[] $classes
   * @param array $called_from
   * @param string $name
   *
   * @return string
   */
  function wrap($string, array $classes, array $called_from = NULL, $name = NULL) {
    $parts = array();
    foreach ($this->classes as $class => $true) {
      $parts[] = "PHP[$class] = function(){};";
    }
    foreach ($this->recursions as $recursion => $true) {
      $parts[] = "Recursion[$recursion] = function(){};";
    }
    $parts = implode("\n", $parts);
    return <<<EOT
(function(){
  function a(data) {
    return o('[]', data);
  }
  function o(classname, data) {
    var obj = new PHP[classname];
    for (var k in data) {
      obj[k] = data[k];
    }
    return obj;
  }
  var PHP = {};
  var Recursion = {};
  PHP['[]'] = function(){};
  $parts
  return $string;
})()
EOT;
  }
}
