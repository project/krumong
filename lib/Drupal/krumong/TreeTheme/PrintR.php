<?php

namespace Drupal\krumong;


/**
 * Render the tree with the same output as print_r().
 */
class TreeTheme_PrintR implements TreeTheme_Interface {

  /**
   * @param array $array
   * @param array $position
   * @param array $children
   *
   * @return string
   */
  function renderArray(array $array, array $position, array $children) {
    $indent = $this->getIndentSpaces($position);
    $text = '';
    foreach ($children as $k => $v) {
      $text .= "\n$indent    [$k] => $v";
    }
    return "Array\n$indent($text\n$indent)\n";
  }

  /**
   * @param object $object
   * @param array $position
   * @param array $children
   *
   * @return string
   */
  function renderObject($object, array $position, array $children) {
    $indent = $this->getIndentSpaces($position);
    $text = '';
    foreach ($children as $k => $v) {
      $text .= "\n$indent    [$k] => $v";
    }
    $class = get_class($object);
    return "$class Object\n$indent($text\n$indent)\n";
  }

  /**
   * @param $position
   *
   * @return string
   */
  protected function getIndentSpaces($position) {
    if (empty($position)) {
      return '';
    }
    else {
      return implode('', array_fill(0, count($position), '        '));
    }
  }

  /**
   * @param resource $resource
   * @param array $position
   *
   * @return mixed|string
   */
  function renderResource($resource, array $position) {
    return print_r($resource, TRUE);
  }

  /**
   * @param mixed $value
   * @param array $position
   *
   * @return mixed|string
   */
  function renderPrimitive($value, array $position) {
    return print_r($value, TRUE);
  }

  /**
   * @param mixed $value
   * @param array $position
   * @param array $original_position
   *
   * @return string
   * @throws \Exception
   */
  function renderRecursion($value, array $position, array $original_position) {
    if (is_object($value)) {
      $class = get_class($value);
      return "$class Object\n *RECURSION*";
    }
    elseif (is_array($value)) {
      return "Array\n *RECURSION*";
    }
    else {
      throw new \Exception('nope');
    }
  }

  /**
   * @param string $string
   * @param string[] $classes
   * @param array $called_from
   * @param string $name
   *
   * @return string
   */
  function wrap($string, array $classes, array $called_from = NULL, $name = NULL) {
    return $string;
  }
}
