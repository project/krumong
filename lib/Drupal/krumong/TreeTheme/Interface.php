<?php

namespace Drupal\krumong;


interface TreeTheme_Interface {

  /**
   * @param array $array
   *   The array at this position.
   * @param array $position
   *   Array representing the current position in the tree.
   * @param array $children
   *   The rendered array values.
   *
   * @return string
   *   The rendered array.
   */
  function renderArray(array $array, array $position, array $children);

  /**
   * @param object $object
   *   The object at this position.
   * @param array $position
   *   Array representing the current position in the tree.
   * @param array $children
   *   The rendered object attribute values.
   *
   * @return string
   *   The rendered object.
   */
  function renderObject($object, array $position, array $children);

  /**
   * @param resource $resource
   *   A resource.
   * @param array $position
   *   Array representing the current position in the tree.
   *
   * @return string
   *   The rendered value.
   */
  function renderResource($resource, array $position);

  /**
   * @param mixed $value
   *   A string or scalar value.
   * @param array $position
   *   Array representing the current position in the tree.
   *
   * @return string
   *   The rendered value.
   */
  function renderPrimitive($value, array $position);

  /**
   * @param mixed $value
   *   The value at this position
   * @param array $position
   *   Array representing the current position in the tree.
   * @param array $original_position
   *   Array representing the tree position where this object/array previously
   *   occured.
   *
   * @return string
   *   A rendered symbole for recursion.
   */
  function renderRecursion($value, array $position, array $original_position);

  /**
   * This method can be used to
   *
   * @param string $string
   *   The rendered tree.
   * @param string[] $classes
   *   Classes used
   * @param array|null $called_from
   *   File and line number of the call to dpm() or similar.
   * @param string $name
   *   Label for the data.
   *
   * @return string
   *   The rendered and wrapped tree.
   */
  function wrap($string, array $classes, array $called_from = NULL, $name = NULL);
}
