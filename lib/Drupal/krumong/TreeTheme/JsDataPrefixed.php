<?php

namespace Drupal\krumong;


/**
 * Render the tree as nested javascript value/object.
 * This is not a simple json_encode(), and it is not valid json, so only works
 * if run as full javascript.
 *
 * Will prefix every array or object key, to avoid name clashes with reserved
 * names such as "__proto__".
 * Additional keys for an object's class.
 */
class TreeTheme_JsDataPrefixed implements TreeTheme_Interface {

  /**
   * @param array $array
   * @param array $position
   * @param array $children
   *
   * @return string
   */
  function renderArray(array $array, array $position, array $children) {
    return $this->renderArrayOrObject($children);
  }

  /**
   * @param object $object
   * @param array $position
   * @param array $children
   *
   * @return string
   */
  function renderObject($object, array $position, array $children) {
    $class = json_encode(get_class($object));
    return $this->renderArrayOrObject($children, array("  class: $class"));
  }

  /**
   * @param $children
   * @param array $parts
   *
   * @return string
   */
  protected function renderArrayOrObject($children, $parts = array()) {
    foreach ($children as $k => $v) {
      $k = json_encode(".$k");
      $parts[] = "  $k: $v";
    }
    return "{\n" . implode(",\n", $parts) . "\n}";
  }

  /**
   * @param resource $resource
   * @param array $position
   *
   * @return string
   */
  function renderResource($resource, array $position) {
    $type = get_resource_type($resource);
    // Use the __toString() in the resource.
    return json_encode("resource ($type): $resource");
  }

  /**
   * @param mixed $value
   * @param array $position
   *
   * @return string
   */
  function renderPrimitive($value, array $position) {
    return json_encode($value);
  }

  /**
   * @param mixed $value
   * @param array $position
   * @param array $original_position
   *
   * @return string
   */
  function renderRecursion($value, array $position, array $original_position) {
    return json_encode(array('recursion' => $original_position));
  }

  /**
   * @param string $string
   * @param string[] $classes
   * @param array $called_from
   * @param string $name
   *
   * @return string
   */
  function wrap($string, array $classes, array $called_from = NULL, $name = NULL) {
    $called_from = json_encode($called_from);
    $name = json_encode($name);
    $classes = json_encode($this->analyzeClasses($classes));
    return "{\n  data: $string,\n  calledFrom: $called_from, name: $name\n, classes: $classes}";
  }

  /**
   * @param string[] $classes
   *
   * @return array
   */
  protected function analyzeClasses(array $classes) {
    $result = array();
    foreach ($classes as $class) {
      $result[$class] = $this->analyzeClass($class);
    }
    return $result;
  }

  /**
   * @param string $class
   *
   * @return array
   */
  protected function analyzeClass($class) {
    $reflectionClass = new \ReflectionClass($class);
    $properties = array();
    foreach ($reflectionClass->getProperties() as $reflectionProperty) {
      $key = $reflectionProperty->getName();
      $properties[$key]['class'] = $reflectionProperty->getDeclaringClass();
      $properties[$key]['prefix'] = $reflectionProperty->isPrivate()
        ? 'private'
        : ($reflectionProperty->isProtected()
          ? 'protected'
          : ($reflectionProperty->isPublic()
            ? 'public'
            : '?'));
    }
    return array(
      'properties' => $properties,
    );
  }
}
