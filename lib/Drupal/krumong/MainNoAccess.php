<?php

namespace Drupal\krumong;


class MainNoAccess extends MainAbstract {

  /**
   * Print data and js/css to stdOut.
   * This can be used on requests that don't have a regular header etc.
   *
   * @param mixed $data
   *   The data to print.
   * @param string $name
   *   Label for the data
   */
  function kPrint($data, $name = NULL) {
    // Do nothing, since the user has no access.
  }

  /**
   * Dump a variable and make it show up in Drupal's message area.
   *
   * @param mixed $data
   *   any variable to dump.
   * @param string $name
   * @param string $type
   */
  function kMessage($data, $name = NULL, $type = 'status') {
    // Do nothing, since the user has no access.
  }

  /**
   * Make the data show up with console.log(), if available in the browser.
   * The script tag will be printed directly to stdOut.
   *
   * @param mixed $data
   * @param string $name
   */
  function jPrint($data, $name = NULL) {
    // Do nothing, since the user has no access.
  }

  /**
   * Make the data show up with console.log(), if available in the browser.
   * The js will be added as inline js with drupal_add_js().
   * It will be postponed to the next request, if necessary.
   *
   * @param mixed $data
   * @param string $name
   */
  function jMessage($data, $name = NULL) {
    // Do nothing, since the user has no access.
  }

  /**
   * Dump a variable and return it as html.
   *
   * @param mixed $data
   *   The data to dump.
   * @param string $name
   *   Label for the data.
   * @param boolean $add_css_js
   *   If TRUE, this will trigger drupal_add_js() / _css().
   *
   * @return string
   *   tree-rendered html
   */
  function dump($data, $name = NULL, $add_css_js = TRUE) {
    // Do nothing, since the user has no access.
    return NULL;
  }

  /**
   * Generate a piece of javascript that will console.log the data as a nested
   * js object tree. It will appear as if the objects have real class names.
   *
   * @param mixed $data
   *   The data to dump.
   * @param string $name
   *   Label for the data.
   *
   * @return string
   *   tree-rendered js.
   */
  function jsPseudoTypedConsoleLog(&$data, $name = NULL) {
    // Do nothing, since the user has no access.
    return NULL;
  }

  /**
   * Generate a piece of javascript that evaluates as a nested object.
   * This is not json, it can only be processed as real js.
   *
   * The returned js has the format that is needed for client-side krumong print.
   *
   * @param mixed $data
   *   The data to dump.
   * @param string $name
   *   Label for the data.
   *
   * @return string
   *   tree-rendered js.
   */
  function jsPrefixed(&$data, $name = NULL) {
    // Do nothing, since the user has no access.
    return NULL;
  }
}
