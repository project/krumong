<?php

namespace Drupal\krumong;


use Drupal\krumong\TreeRenderer\Hive\Hive;

abstract class TreeRenderer_Abstract {

  /**
   * @var TreeTheme_Interface
   */
  protected $theme;

  /**
   * @var string[]
   *   Array of keys representing the current position in the nested data
   *   structure that is being rendered.
   */
  protected $trailOfKeys = array();

  /**
   * @var Hive
   *   Keeps track of objects and arrays that have already been visited
   *   somewhere in the nested data structure.
   */
  protected $hive;

  /**
   * @var true[]
   *   Classes used in the nested data structure.
   */
  protected $classes = array();

  /**
   * @param TreeTheme_Interface $theme
   *   "Theme" to use for the rendering.
   */
  function __construct(TreeTheme_Interface $theme) {
    $this->theme = $theme;
  }

  /**
   * Render a (nested) data structure (, or a simple value).
   *
   * @param mixed $data
   *   Any value, e.g. nested array etc.
   * @param array $called_from
   *   File and line number of the call to dpm() or similar.
   * @param string $name
   *
   * @return string
   *   Rendered and "wrapped" output.
   */
  function render(&$data, $called_from = NULL, $name = NULL) {

    // Initialize.
    $this->classes = array();
    $this->trailOfKeys = array();
    $this->initHive($data);

    // Render
    $result = $this->renderData($data);
    $result = $this->theme->wrap($result, array_keys($this->classes), $called_from, $name);

    // Clean up and return.
    $this->hive->clear();
    return $result;
  }

  /**
   * Render a value and everything that's nested below.
   *
   * @param mixed $data
   *   Any value, e.g. nested array etc.
   *
   * @throws \Exception
   * @return string
   *   Rendered output.
   */
  protected function renderData(&$data) {

    if (is_object($data) || is_array($data)) {
      $info = $this->hive->getElementInfo($data);
    }
    elseif (is_resource($data)) {
      return $this->theme->renderResource($data, $this->trailOfKeys);
    }
    else {
      return $this->theme->renderPrimitive($data, $this->trailOfKeys);
    }

    // From here on, $data is known to be an object or array.
    if ($this->hiveDetectRecursion($data, $info)) {
      // Recursion was detected.
      if (!is_array($info) || !is_array($info['trail'])) {
        throw new \Exception('$info["trail"] must be array.');
      }
      // Stop recursion.
      return $this->theme->renderRecursion($data, $this->trailOfKeys, $info['trail']);
    }
    else {
      // No recursion was detected. Process children.
      return $this->renderNestedData($data);
    }
  }

  /**
   * Render all array values or object attribute values, and return them as an
   * associative array.
   *
   * @param array|object $data
   *   An array or object, the values or attributes of which we are interested
   *   in.
   *
   * @return array
   *   Rendered output, one string for each array value / object attribute.
   */
  protected function renderNestedData(&$data) {
    if (is_object($data)) {
      $rendered = $this->renderObject($data);
      return $rendered;
    }
    else {
      return $this->renderArray($data);
    }
  }

  /**
   * @param object $object
   *
   * @return string
   * @throws \Exception
   */
  protected function renderObject($object) {

    // Remember the class.
    $this->classes[get_class($object)] = TRUE;

    $children = array();
    $reflection = new \ReflectionObject($object);
    foreach ($reflection->getProperties() as $property) {
      $prefix = null;
      $setAccessible = false;
      $key = $property->getName();

      if ($property->isPrivate() || $property->isProtected()) {
        $setAccessible = true;
      }

      // Get the property value. This requires to make it public temporarily.
      if ($setAccessible) {
        $property->setAccessible(true);
      }
      $value = $property->getValue($object);
      if ($setAccessible) {
        $property->setAccessible(false);
      }

      // Render the property value.
      $this->trailOfKeys[] = $key;
      $children[$key] = $this->renderData($value);
      array_pop($this->trailOfKeys);
    }

    return $this->theme->renderObject($object, $this->trailOfKeys, $children);
  }

  /**
   * @param $array
   *
   * @return string
   */
  protected function renderArray(&$array) {
    $children = array();
    foreach ($array as $key => &$value) {
      if ($key === $this->hive->getMarkerKey()) {
        continue;
      }
      $this->trailOfKeys[] = $key;
      $children[$key] = $this->renderData($value);
      array_pop($this->trailOfKeys);
    }

    return $this->theme->renderArray($array, $this->trailOfKeys, $children);
  }

  /**
   * @param mixed $data
   * @param array|false $info
   *
   * @return bool
   */
  abstract protected function hiveDetectRecursion(&$data, $info);

  /**
   * @param mixed $data
   */
  protected function initHive(&$data) {
    $markerKey = '_' . Util::randomstring(40);
    $this->hive = new Hive($markerKey);
  }
}
