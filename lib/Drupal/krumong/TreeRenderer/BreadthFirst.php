<?php

namespace Drupal\krumong;


class TreeRenderer_BreadthFirst extends TreeRenderer_Abstract {

  /**
   * @param mixed $data
   * @param array|false $info
   *
   * @return bool
   */
  protected function hiveDetectRecursion(&$data, $info) {
    if (FALSE === $info) {
      // This cannot happen!
      return FALSE;
    }
    elseif (serialize($info['trail']) === serialize($this->trailOfKeys)) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * @param mixed $data
   *
   * @return \Drupal\krumong\TreeRenderer\Hive\Hive
   */
  protected function initHive(&$data) {
    // Set the internal key.
    parent::initHive($data);
    // Walk through the data once before the rendering process,
    // to initialize the hive.
    $this->hiveAddRecursive($data);
  }

  /**
   * @param mixed $data
   */
  protected function hiveAddRecursive(&$data) {

    if (!is_object($data) && !is_array($data)) {
      // This element is not an array or object.
      // No recursion.
      return;
    }

    $info = $this->hive->getElementInfo($data);

    // From here on, $data is known to be an object or array.
    if (FALSE === $info) {
      // We visit this for the first time.
      $this->hive->addElement($data, $this->trailOfKeys);
      $this->hiveAddChildren($data);
    }
    elseif (count($info['trail']) <= count($this->trailOfKeys)) {
      // This position is not closer to root than the exisiting one.
      // Stop recursion.
    }
    else {
      // We found a position that is closer to root.
      $info['trail'] = $this->trailOfKeys;
      // Children need update, because they all move closer to root.
      $this->hiveAddChildren($data);
    }
  }

  /**
   * @param mixed $data
   */
  protected function hiveAddChildren(&$data) {
    foreach ($data as $k => &$v) {
      if ($k === $this->hive->getMarkerKey()) {
        continue;
      }
      $this->trailOfKeys[] = $k;
      $this->hiveAddRecursive($v);
      array_pop($this->trailOfKeys);
    }
  }
}
