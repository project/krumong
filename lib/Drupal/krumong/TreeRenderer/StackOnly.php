<?php

namespace Drupal\krumong;


/**
 * A tree renderer which does the recursion check only on the active stack.
 */
class TreeRenderer_StackOnly extends TreeRenderer_DepthFirst {

  /**
   * @param array|object $data
   *
   * @return array
   */
  protected function renderNestedData(&$data) {
    $rendered = parent::renderNestedData($data);
    // Forget the element when we are done rendering its children.
    $this->hive->forgetElement($data, $this->trailOfKeys);
    return $rendered;
  }
}
