<?php

namespace Drupal\krumong;


class TreeRenderer_DepthFirst extends TreeRenderer_Abstract {

  /**
   * @param mixed $data
   * @param array|false $info
   *
   * @return bool
   */
  protected function hiveDetectRecursion(&$data, $info) {
    if (FALSE === $info) {
      $this->hive->addElement($data, $this->trailOfKeys);
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}
