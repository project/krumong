<?php


namespace Drupal\krumong\TreeRenderer\Hive;

class Hive {

  /**
   * @var string
   */
  protected $markerKey;

  /**
   * @var HiveObjects
   */
  protected $hiveObjects;

  /**
   * @var HiveArrays
   */
  protected $hiveArrays;

  /**
   * @param string $markerKey
   */
  function __construct($markerKey) {
    $this->markerKey = $markerKey;
    $this->hiveObjects = new HiveObjects();
    $this->hiveArrays = new HiveArrays($markerKey);
  }

  function clear() {
    $this->hiveArrays->clear();
  }

  /**
   * @return string
   */
  function getMarkerKey() {
    return $this->markerKey;
  }

  /**
   * @param mixed $data
   *
   * @return array|false
   */
  function getElementInfo(&$data) {

    if (is_object($data)) {
      return $this->hiveObjects->getObjectInfo($data);
    }
    elseif (is_array($data)) {
      return $this->hiveArrays->getArrayInfo($data);
    }
    else {
      // This element is not an array or object. No recursion.
      return FALSE;
    }
  }

  /**
   * @param mixed $data
   * @param array $trail
   */
  function addElement(&$data, array $trail) {

    if (is_object($data)) {
      $this->hiveObjects->addObject($data, $trail);
    }
    elseif (is_array($data)) {
      $this->hiveArrays->addArray($data, $trail);
    }
  }

  /**
   * @param mixed $data
   * @param array $trail
   */
  function forgetElement(&$data, array $trail) {

    if (is_object($data)) {
      $this->hiveObjects->forgetObject($data, $trail);
    }
    elseif (is_array($data)) {
      $this->hiveArrays->forgetArray($data, $trail);
    }
  }
} 
