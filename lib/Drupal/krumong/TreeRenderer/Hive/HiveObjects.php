<?php


namespace Drupal\krumong\TreeRenderer\Hive;


class HiveObjects {

  /**
   * @var array
   */
  protected $objects = array();

  /**
   * @param object $obj
   *
   * @return array|false
   */
  function getObjectInfo($obj) {

    $class = get_class($obj);
    if (isset($this->objects[$class])) {
      foreach ($this->objects[$class] as &$existing) {
        if ($obj === $existing[0]) {
          return array('trail' => &$existing[1]);
        }
      }
    }

    return FALSE;
  }

  /**
   * @param object $obj
   * @param array $trail
   */
  function addObject($obj, array $trail) {

    $class = get_class($obj);
    $key = serialize($trail);
    $this->objects[$class][$key] = array($obj, $trail);
  }

  /**
   * @param object $obj
   * @param array $trail
   */
  function forgetObject($obj, array $trail) {

    $class = get_class($obj);
    $key = serialize($trail);
    unset($this->objects[$class][$key]);
  }
} 
