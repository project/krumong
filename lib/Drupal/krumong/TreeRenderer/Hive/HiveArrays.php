<?php


namespace Drupal\krumong\TreeRenderer\Hive;


class HiveArrays {

  /**
   * @var string
   */
  protected $markerKey;

  /**
   * @var array[]
   */
  protected $arrays = array();

  /**
   * @param string $markerKey
   */
  function __construct($markerKey) {
    $this->markerKey = $markerKey;
  }

  function clear() {
    foreach ($this->arrays as &$arr) {
      unset($arr[$this->markerKey]);
    }
  }

  /**
   * @param array $arr
   *
   * @return array|false
   */
  function getArrayInfo(array &$arr) {
    if (isset($arr[$this->markerKey])) {
      // The array is already visited.
      return array('trail' => &$arr[$this->markerKey]);
    }

    return FALSE;
  }

  /**
   * @param array $arr
   * @param array $trail
   */
  function addArray(array &$arr, array $trail) {

    $key = serialize($trail);
    $arr[$this->markerKey] = $trail;
    $this->arrays[$key] =& $arr;
  }

  function forgetArray(array &$arr, array $trail) {

    $key = serialize($trail);
    unset($this->arrays[$key]);
    unset($arr[$this->markerKey]);
  }
} 
