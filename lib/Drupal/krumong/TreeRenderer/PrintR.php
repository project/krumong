<?php

namespace Drupal\krumong;


/**
 * A tree renderer which does the recursion check exactly like print_r().
 */
class TreeRenderer_PrintR extends TreeRenderer_StackOnly {

  /**
   * @param mixed $data
   * @param array|false $info
   *
   * @return bool
   */
  protected function hiveDetectRecursion(&$data, $info) {
    if (FALSE === $info) {
      $this->hive->addElement($data, $this->trailOfKeys);
      return FALSE;
    }
    elseif (empty($info['trail'])) {
      // print_r() does not stop on top-level recursion.
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}
