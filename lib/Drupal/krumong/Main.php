<?php

namespace Drupal\krumong;


use Drupal\krumong\Output\CssJs;
use Drupal\krumong\Output\SessionRequestCache;
use Drupal\krumong\Output\StdOut;
use Drupal\krumong\ServiceCache\ServiceCache;

class Main extends MainAbstract {

  /**
   * @var SessionRequestCache
   */
  protected $requestCache;

  /**
   * @var StdOut
   */
  protected $stdOut;

  /**
   * @var CssJs
   */
  protected $cssJs;

  /**
   * @param ServiceCache $services
   * @param SessionRequestCache $request_cache
   *   Service for drupal_set_message() and delayed drupal_add_js() / _css().
   * @param StdOut $stdOut
   * @param CssJs $cssJs
   */
  function __construct(ServiceCache $services, SessionRequestCache $request_cache, StdOut $stdOut, CssJs $cssJs) {
    $this->requestCache = $request_cache;
    $this->stdOut = $stdOut;
    $this->cssJs = $cssJs;
    parent::__construct($services);
  }

  /**
   * Print data and js/css to stdOut.
   * This can be used on requests that don't have a regular header etc.
   *
   * @param mixed $data
   *   The data to print.
   * @param string $name
   *   Label for the data
   */
  function kPrint($data, $name = NULL) {
    $html = $this->dump($data, $name, FALSE);
    if (isset($html)) {
      $this->stdOut->html($html);
    }
  }

  /**
   * Dump a variable and make it show up in Drupal's message area.
   *
   * @param mixed $data
   *   any variable to dump.
   * @param string $name
   * @param string $type
   */
  function kMessage($data, $name = NULL, $type = 'status') {
    $html = $this->dump($data, $name, FALSE);
    $this->requestCache->message($html, $type);
  }

  /**
   * Make the data show up with console.log(), if available in the browser.
   * The script tag will be printed directly to stdOut.
   *
   * @param mixed $data
   * @param string $name
   */
  function jPrint($data, $name = NULL) {
    $js = $this->jsPseudoTypedConsoleLog($data, $name);
    $html = <<<EOT
<script type="text/javascript"> // <!--
$js
// --></script>
EOT;
    $this->stdOut->html($html);
  }

  /**
   * Make the data show up with console.log(), if available in the browser.
   * The js will be added as inline js with drupal_add_js().
   * It will be postponed to the next request, if necessary.
   *
   * @param mixed $data
   * @param string $name
   */
  function jMessage($data, $name = NULL) {
    $js = $this->jsPseudoTypedConsoleLog($data, $name);
    $this->requestCache->jsInline($js);
  }

  /**
   * Dump a variable and return it as html.
   *
   * @param mixed $data
   *   The data to dump.
   * @param string $name
   *   Label for the data.
   * @param boolean $add_css_js
   *   If TRUE, this will trigger drupal_add_js() / _css().
   *
   * @return string
   *   tree-rendered html
   */
  function dump($data, $name = NULL, $add_css_js = TRUE) {
    if ($add_css_js) {
      $this->cssJs->addCssJsOnce();
    }
    $js = $this->jsPrefixed($data, $name);
    $randomstring = Util::randomstring(30);
    $html = <<<EOT
<script type="text/javascript"> // <!--
jQuery(function(){
  var data = $js;
  krumong.krumong('#_$randomstring', data.classes, data.data, data.calledFrom);
});
// --></script>
<div id="_$randomstring"></div>
EOT;
    if (isset($name)) {
      $name = check_plain($name);
      $html = <<<EOT
$name:
$html
EOT;
    }
    return $html;
  }

  /**
   * Generate a piece of javascript that will console.log the data as a nested
   * js object tree. It will appear as if the objects have real class names.
   *
   * @param mixed $data
   *   The data to dump.
   * @param string $name
   *   Label for the data.
   *
   * @return string
   *   tree-rendered js.
   */
  function jsPseudoTypedConsoleLog(&$data, $name = NULL) {
    $theme = new TreeTheme_JsDataPseudoTyped();
    $js = $this->renderData($data, $theme);
    return <<<EOT
(function(){
  var msg = $js;
  console && console.log && console.log(msg);
})();
EOT;
  }

  /**
   * Generate a piece of javascript that evaluates as a nested object.
   * This is not json, it can only be processed as real js.
   *
   * The returned js has the format that is needed for client-side krumong print.
   *
   * @param mixed $data
   *   The data to dump.
   * @param string $name
   *   Label for the data.
   *
   * @return string
   *   tree-rendered js.
   */
  function jsPrefixed(&$data, $name = NULL) {
    $theme = new TreeTheme_JsDataPrefixed();
    return $this->renderData($data, $theme);
  }

  /**
   * Let the tree renderer render a nested data using a TreeTheme.
   *
   * @param mixed $data
   *   The data to dump.
   * @param TreeTheme_Interface $theme
   *   The theme to use.
   * @param string $name
   *   Label for the data.
   *
   * @return string
   *   tree-rendered js.
   */
  protected function renderData(&$data, TreeTheme_Interface $theme, $name = NULL) {
    $renderer = new TreeRenderer_BreadthFirst($theme);
    return $renderer->render($data, $this->calledFrom(), $name);
  }
}
