<?php


namespace Drupal\krumong;

/**
 * @param mixed $data
 * @param null $name
 */
function dpm($data, $name = NULL) {
  krumong()->devel->dpm($data, $name);
}
